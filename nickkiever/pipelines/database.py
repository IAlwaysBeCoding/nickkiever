from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, ForeignKey, Sequence, Integer, \
                        String, Text, Boolean, DateTime, create_engine

from sqlalchemy.orm import scoped_session, sessionmaker

from scrapy.utils.project import get_project_settings
from scrapy.signals import engine_stopped

#from nickkiever.items import StockItem

SETTINGS = get_project_settings()
DATABASE = 'mysql+pymysql://{}:{}@{}:{}/{}'.format(
        SETTINGS['MYSQL_USERNAME'], SETTINGS['MYSQL_PASSWORD'],
        SETTINGS['MYSQL_HOST'], SETTINGS['MYSQL_PORT'] , SETTINGS['MYSQL_DATABASE'])

Base = declarative_base()
engine = create_engine(DATABASE, convert_unicode=True, echo=False)


class DatabaseException(Exception):
    pass

class Stock(Base):

    __tablename__   = "stock"

    id              = Column(Integer, autoincrement=True, primary_key=True)
    site            = Column(String(32))
    site_url        = Column(String(128))
    product_url     = Column(String(2048))
    stock_qty       = Column(Integer)
    size            = Column(String(6))
    size_type       = Column(String(18))
    price           = Column(String(10))
    updated_at      = Column(DateTime)


class APIKey(Base):

    __tablename__   = "apikeys"

    id                = Column(Integer, autoincrement=True, primary_key=True)
    site              = Column(String(32))
    client_id         = Column(String(128))
    client_secret_key = Column(String(128))
    api_token         = Column(String(128))


Base.metadata.create_all(engine)

DBSession = scoped_session(sessionmaker())
DBSession.configure(bind=engine, autoflush=False, expire_on_commit=False)


class DatabaseExporter(object):

    @classmethod
    def from_crawler(cls, crawler):

        crawler.signals.connect(cls.close_db_session, signal=engine_stopped)

        return cls()

    @staticmethod
    def close_db_session():
        DBSession.close()

    def process_item(self, item, spider):

        self.process_stock(item)

        return item

    def process_stock(self, item):

        """
        has_stock = DBSession.query(Stock) \
            .filter_by(site=item['site']) \
            .filter_by(product_url=item['product_url'])\
        """

        stock_item = Stock(**item)
        DBSession.add(stock_item)

        try:
            DBSession.commit()
        except Exception as e:
            DBSession.rollback()
            raise DatabaseException(e)


