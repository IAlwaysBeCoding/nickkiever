# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

from scrapy import Item, Field


class StockItem(Item):
    id              = Field()
    site            = Field()
    site_url        = Field()
    product_url     = Field()
    stock_qty       = Field()
    size            = Field()
    size_type       = Field()
    price           = Field()
    updated_at      = Field()



