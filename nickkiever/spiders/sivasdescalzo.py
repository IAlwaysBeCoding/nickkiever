
import json
import re
import time
from datetime import timedelta, datetime

import arrow
from furl import furl

from scrapy import Spider, Request, Selector

from nickkiever.items import StockItem


class SivasdescalzoSpider(Spider):
    name = 'sivasdescalzo'
    allowed_domains = ['sivasdescalzo.com']
    start_urls = ['https://www.sivasdescalzo.com/en/lifestyle/sneakers']

    def parse(self, response):

        for prod in response.xpath('//ul[contains(@class,"products-list")]/li').extract():
            psel = Selector(text=prod)
            url = psel.xpath('/descendant::a[3]/@href').extract_first()
            if url:
                yield Request(url,
                            callback=self.parse_product_detail)

        next_page = response.xpath('//a[@title="Next page"]/@href').extract_first()

        if next_page:

            yield Request(next_page,
                          callback=self.parse)

    def parse_product_detail(self, response):

        now = datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')

        quantities = []

        for stype in response.xpath('//div[contains(@class, "content size-options")]').extract():
            self.log('yes')
            ssel = Selector(text=stype)

            size_type = ssel.xpath('//*[@data-sizegroup]/@data-sizegroup').extract_first()
            if size_type == 'size_us':
                size_type = 'us'
            elif size_type == 'size_eu':
                size_type = 'eu'
            elif size_type == 'size_uk':
                size_type = 'uk'
            elif size_type == 'size_cm':
                size_type = 'cm'

            stock_qty = ssel.xpath('//a[@rel="nofollow" and @class="size-button available"]/@data-qty').extract_first()
            size = ssel.xpath('//a[@rel="nofollow" and @class="size-button available"]/text()').extract_first()

            stock_item = StockItem()
            stock_item['site'] = self.name
            stock_item['site_url'] = self.allowed_domains[0]
            stock_item['product_url'] = response.url
            stock_item['stock_qty'] = stock_qty
            stock_item['size'] = size
            stock_item['size_type'] = size_type

            find_price = re.search(b'(?<="price": ")(.*?)(?:")', response.body, re.DOTALL)
            if find_price:
                price  = find_price.group(0)
            else:
                price = ''

            stock_item['updated_at'] = now
            stock_item['price'] = price

            yield stock_item



